/**
 * Zamenjava kode smeška s sliko (iz oddaljenega strežnika
 * https://sandbox.lavbic.net/teaching/OIS/gradivo/{smesko}.png)
 * 
 * @param vhodnoBesedilo sporočilo, ki lahko vsebuje kodo smeška
 */
 
function dodajSmeske(vhodnoBesedilo) {
  var preslikovalnaTabela = {
    ";)": "wink.png",
    ":)": "smiley.png",
    "(y)": "like.png",
    ":*": "kiss.png",
    ":(": "sad.png"
  }
  for (var smesko in preslikovalnaTabela) {
    vhodnoBesedilo = vhodnoBesedilo.split(smesko).join(
      "<img src='https://sandbox.lavbic.net/teaching/OIS/gradivo/" +
      preslikovalnaTabela[smesko] + "' />");
  }
  return vhodnoBesedilo;
}


/**
 * Čiščenje besedila sporočila z namenom onemogočanja XSS napadov
 * 
 * @param sporocilo začetno besedilo sporočila
 */
function divElementEnostavniTekst(sporocilo) {
         
    preim = sporocilo;
    
      if( preim.indexOf("se je preimenoval v") > -1){

    var kkk = nadimek;
    
   preim = preim.split(" ");
      preim[5] = preim[5].substring(0, preim[5].length-1);
         nadimek[preim[5]] = nadimek[preim[0]];
    
   }

  
  var jeSmesko = sporocilo.indexOf("https://sandbox.lavbic.net/teaching/OIS/gradivo/") > -1;
  
  var loci = sporocilo.split(" ");
  var neki = false; 
  
  for (var i = 0; i < loci.length ; i ++){
    if (loci[i].startsWith("http") && (   loci[i].endsWith(".gif") || loci[i].endsWith(".png") || loci[i].endsWith(".jpg")  ||  loci[i].endsWith(".gif.") || loci[i].endsWith(".gif?") || loci[i].endsWith(".gif!") || loci[i].endsWith(".png.") || loci[i].endsWith(".png!") || 
                                               loci[i].endsWith(".png?") || loci[i].endsWith(".jpg.") ||   loci[i].endsWith(".jpg?") || loci[i].endsWith(".jpg!")     ) ){
      
      neki = true; 
    }
    
    
  }
  
  
  if (jeSmesko || neki) {
    sporocilo = 
      sporocilo.split("<").join("&lt;")
               .split(">").join("&gt;")
               .split("&lt;img").join("<img")
               .split("png' /&gt;").join("png' />");
               
               
               var novo;
               
    for (var i = 0; i < loci.length ; i ++){
      
    if (loci[i].startsWith("http") && (   loci[i].endsWith(".gif") || loci[i].endsWith(".png") || loci[i].endsWith(".jpg")    )){
      
      
  
  
      sporocilo +=   "<br/><img src=" + loci[i] + " style='width: 200px; padding-left=20px;' >"; 
      console.log (sporocilo);
    }
   
    else if (loci[i].startsWith("http") && (   loci[i].endsWith(".gif.") || loci[i].endsWith(".gif?") || loci[i].endsWith(".gif!") || loci[i].endsWith(".png.") || loci[i].endsWith(".png!") || 
                                               loci[i].endsWith(".png?") ||
                                               loci[i].endsWith(".jpg.") ||
                                               loci[i].endsWith(".jpg?") ||
                                               loci[i].endsWith(".jpg!") ||
                                               loci[i].endsWith(".jpg,") ||
                                               loci[i].endsWith(".jpg:") ||
                                               loci[i].endsWith(".png,") ||
                                               loci[i].endsWith(".gif,") ||
                                               loci[i].endsWith(".png:") ||
                                               loci[i].endsWith(".gif:") ) )    {
                                                 
        var x = loci [i].substring (0, loci[i].length-1);
         sporocilo+=   "<br/><img src=" + x + " style='width: 200px; padding-left=20px;' >"; 
                          
    }
    
  }
    
    return divElementHtmlTekst(sporocilo);
    
  } else {
    return $('<div style="font-weight: bold"></div>').text(sporocilo);  
  }
    
}


/**
 * Prikaz "varnih" sporočil, t.j. tistih, ki jih je generiral sistem
 * 
 * @param sporocilo začetno besedilo sporočila
 */
function divElementHtmlTekst(sporocilo) {
  return $('<div></div>').html('<i>' + sporocilo + '</i>');
}


/**
 * Obdelaj besedilo, ki ga uporabnik vnese v obrazec na spletni strani, kjer
 * je potrebno ugotoviti ali gre za ukaz ali za sporočilo na kanal
 * 
 * @param klepetApp objekt Klepet, ki nam olajša obvladovanje 
 *        funkcionalnosti uporabnika
 * @param socket socket WebSocket trenutno prijavljenega uporabnika
 */
function procesirajVnosUporabnika(klepetApp, socket) {
  var sporocilo = $('#poslji-sporocilo').val();
  sporocilo = dodajSmeske(sporocilo);
  var sistemskoSporocilo;
    



  // Če uporabnik začne sporočilo z znakom '/', ga obravnavaj kot ukaz
  if (sporocilo.charAt(0) == '/') {
    
              /////////////////////////////////////////
  if (sporocilo.charAt(1) == 'p' && sporocilo.charAt(2) == 'r' && sporocilo.charAt(3) == 'e' && 
        sporocilo.charAt(4) == 'i' && sporocilo.charAt(5) == 'm' && sporocilo.charAt(6) == 'e' && sporocilo.charAt(7) == 'n' && sporocilo.charAt(8) == 'u'
          && sporocilo.charAt(9) == 'j'){ 
            

            xx = sporocilo;
             xx = xx.split (' ');
             xx.shift();
              xxx = xx.join(' ');
              xxxx = xxx.split('\"');
       
    
       if (xxxx[1] != trenutniVzdevek)
       nadimek[xxxx[1]] = xxxx[3];
       
       
      
          } 

    sistemskoSporocilo = klepetApp.procesirajUkaz(sporocilo);
    if (sistemskoSporocilo) {
      $('#sporocila').append(divElementHtmlTekst(sistemskoSporocilo));
    }
  // Če gre za sporočilo na kanal, ga posreduj vsem članom kanala
          
  } else {
    sporocilo = filtirirajVulgarneBesede(sporocilo);
    klepetApp.posljiSporocilo(trenutniKanal, sporocilo);
    $('#sporocila').append(divElementEnostavniTekst(sporocilo));
    $('#sporocila').scrollTop($('#sporocila').prop('scrollHeight'));
  }

  $('#poslji-sporocilo').val('');
}

// Branje vulgarnih besed v seznam
var vulgarneBesede = [];
$.get('/swearWords.txt', function(podatki) {
  vulgarneBesede = podatki.split('\r\n'); 
});

/**
* Iz podanega niza vse besede iz seznama vulgarnih besed zamenjaj z enako dolžino zvezdic (*).
* 
* @param vhodni niz
*/
function filtirirajVulgarneBesede(vhod) {

 for (var i in vulgarneBesede) {
   var re = new RegExp('\\b' + vulgarneBesede[i] + '\\b', 'gi');
   vhod = vhod.replace(re, "*".repeat(vulgarneBesede[i].length));
 }
 
 return vhod;
}


var socket = io.connect();
var trenutniVzdevek = "";
var trenutniKanal = "";


var xxx;
var xxxx = [];
var xx;
var nadimek = []; /* global nadimek  */
var preim;



// Poačakj, da se celotna stran naloži, šele nato začni z izvajanjem kode
$(document).ready(function() {
  var klepetApp = new Klepet(socket);
  
  // Prikaži rezultat zahteve po spremembi vzdevka
  socket.on('vzdevekSpremembaOdgovor', function(rezultat) {
    var sporocilo;
    if (rezultat.uspesno) {
      trenutniVzdevek = rezultat.vzdevek;
      $('#kanal').text(trenutniVzdevek + " @ " + trenutniKanal);
      sporocilo = 'Prijavljen si kot ' + rezultat.vzdevek + '.';
    } else {
      sporocilo = rezultat.sporocilo;
    }
    $('#sporocila').append(divElementHtmlTekst(sporocilo));
  });
  
  // Prikaži rezultat zahteve po spremembi kanala
  socket.on('pridruzitevOdgovor', function(rezultat) {
    trenutniKanal = rezultat.kanal;
    $('#kanal').text(trenutniVzdevek + " @ " + trenutniKanal);
    $('#sporocila').append(divElementHtmlTekst('Sprememba kanala.'));
  });
  
  // Prikaži prejeto sporočilo
  socket.on('sporocilo', function (sporocilo) {
    var novElement = divElementEnostavniTekst(sporocilo.besedilo);
    $('#sporocila').append(novElement);
    
 
  });
  
  
  // Prikaži seznam kanalov, ki so na voljo
  socket.on('kanali', function(kanali) {
    $('#seznam-kanalov').empty();
    
    for(var i in kanali) {
      if (kanali[i] != '') {
        $('#seznam-kanalov').append(divElementEnostavniTekst(kanali[i]));
      }
    }
  
    // Klik na ime kanala v seznamu kanalov zahteva pridružitev izbranemu kanalu
    $('#seznam-kanalov div').click(function() {
      klepetApp.procesirajUkaz('/pridruzitev ' + $(this).text());
      $('#poslji-sporocilo').focus();
    });
  });
  
  
  // Prikaži seznam trenutnih uporabnikov na kanalu
  socket.on('uporabniki', function(uporabniki) {
    $('#seznam-uporabnikov').empty();
    
    
    var print =[] ; 
   for (var i = 0; i < uporabniki.length; i++){
     if (nadimek.hasOwnProperty(uporabniki[i]) && nadimek[uporabniki[i]] != undefined){
      

       print[i] = nadimek[uporabniki[i]] +  " ("+uporabniki[i] + ")";


     }
     else {
       print[i] = uporabniki[i];
     }
   }
    

 
    
    for (var i=0; i < uporabniki.length; i++) {
       if (uporabniki[i] != '') {
      $('#seznam-uporabnikov').append(divElementEnostavniTekst(print[i]));
       }
    }
    
     // krcanje
    $('#seznam-uporabnikov div').click(function() {
      var lala =$(this).text();
      var xdxd = lala.split(" ");
        console.log(xdxd);
        var zzz;
        for (var i = 0; i < xdxd.length;i++){
          if (xdxd[i].startsWith("(") && xdxd[i].endsWith(")") ){
            xdxd[i] = xdxd[i].substring(1, xdxd[i].length-1);
             zzz = xdxd[i];
          }
        }
        if (zzz != undefined){
          lala = zzz;
        }
        
      
      
      klepetApp.procesirajUkaz('/zasebno ' + '\"'+ lala +'\" ' + "\"☜\"");
      $('#poslji-sporocilo').focus();
      $('#sporocila').append(divElementHtmlTekst('(zasebno za ' + $(this).text() +'): ☜'));
    });
  });
  
  
  
  
  // Seznam kanalov in uporabnikov posodabljaj vsako sekundo
  setInterval(function() {
    socket.emit('kanali');
    socket.emit('uporabniki', {kanal: trenutniKanal});
  }, 1000);

  $('#poslji-sporocilo').focus();
  
  // S klikom na gumb pošljemo sporočilo strežniku
  $('#poslji-obrazec').submit(function() {
    procesirajVnosUporabnika(klepetApp, socket);
    return false;
  });
});
